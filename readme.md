### Java VoipQ API Client ###
A java client for the VoipQ reseller API using 
[JBoss RestEasy client JAX-RS framework](https://docs.jboss.org/resteasy/docs/3.0.16.Final/userguide/html/RESTEasy_Client_Framework.html).
RestEasy has no configuration and constructs a client in 3 lines. But there are several
alternatives like using bare
[Apache HTTP client libraries](https://hc.apache.org/httpcomponents-client-ga/index.html)
with Jackson json mapping or the
[Jersey REST client API](https://jersey.java.net/documentation/latest/client.html).


## Requirements ##
1. Java 8 and maven installed
1. Your [VoipQ](https://voipq.nl) Reseller API credentials

## Quick start ##
```
cd your/project/folder
git clone https://bitbucket.org/voipq/voipq-api-javaclient.git
cd voipq-api-javaclient
cp src/main/resources/config-sample.properties src/main/resources/config.properties
vi src/main/resources/config.properties
mvn clean install
```

## Packages ##

- nl.voipq.api.demo: Ready to run cmd line standalone tests
- nl.voipq.api.domain: All the domain entities like Customer and DidNumber
- nl.voipq.api.impl: Very basic and lightweight RestEasy client implementation
- nl.voipq.api.resource: API Interface definitions