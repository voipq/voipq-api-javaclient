package nl.voipq.api.resource;

import nl.voipq.api.domain.CallDetailResponse;
import nl.voipq.api.domain.CallDetailSearchCriteria;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/cdr")
public interface CallDetailResource {

    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    List<CallDetailResponse> search(CallDetailSearchCriteria userCriteria);
}
