package nl.voipq.api.resource;

import nl.voipq.api.domain.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/didnumber")
public interface DidNumberResource {

    @GET
    @Path("/regions/{isoCountryCode}")
    List<Region> findRegionsForCountry(@PathParam("isoCountryCode") String isoCountryCode);

    @POST
    @Path("offer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    List<DidNumber> offer(DidNumberOffer offer);

    @POST
    @Path("request")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    DidNumber request(DidNumberRequest request);


    @POST
    @Path("activate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    DidNumber activate(DidNumberActivate request) throws NotFoundException;

}
