package nl.voipq.api.resource;

import nl.voipq.api.domain.PortingRequest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/numberporting")
public interface NumberPortingResource {

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    List<PortingRequest> findAll();
}
