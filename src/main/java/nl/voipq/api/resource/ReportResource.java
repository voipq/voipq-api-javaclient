package nl.voipq.api.resource;

import nl.voipq.api.domain.SalesInvoice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/report")
public interface ReportResource {

    @GET
    @Path("/salesinvoices/{year}/{month}")
    @Produces(MediaType.APPLICATION_JSON)
    List<SalesInvoice> listSalesInvoices(
            @PathParam("year") final Integer year,
            @PathParam("month") final Integer month);
}
