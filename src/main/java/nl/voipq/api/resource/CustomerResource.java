package nl.voipq.api.resource;

import nl.voipq.api.domain.CustomerDetail;
import nl.voipq.api.domain.CustomerInfo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/customer")
public interface CustomerResource {

    @GET
    @Path("/{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    CustomerDetail getCustomer(@PathParam("uuid") String uuid);

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    List<CustomerInfo> listCustomers();
}