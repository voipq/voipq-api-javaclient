package nl.voipq.api.resource;

import nl.voipq.api.domain.AccountDetail;
import nl.voipq.api.domain.CustomerDetail;
import nl.voipq.api.domain.CustomerInfo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/account")
public interface AccountResource {

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    List<AccountDetail> listAccounts();
}