package nl.voipq.api.resource;

import nl.voipq.api.domain.EchoResponse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/test")
public interface TestResource {

    @GET
    @Path("/echo/{message}")
    @Produces(MediaType.APPLICATION_JSON)
    public EchoResponse doEcho(@PathParam("message") String message);
}
