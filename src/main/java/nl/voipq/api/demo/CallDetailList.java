package nl.voipq.api.demo;


import nl.voipq.api.domain.CallDetailSearchCriteria;
import nl.voipq.api.impl.VoipqApiClient;

import java.io.IOException;

public class CallDetailList {
    public static void main(String[] args) {

        CallDetailSearchCriteria criteria = new CallDetailSearchCriteria();
        criteria.setYear(2017);
        criteria.setMonth(1);
        criteria.setDay(10);

        new VoipqApiClient()
                .getCallDetailResource()
                .search(criteria)
                .forEach(System.out::println);
    }

}
