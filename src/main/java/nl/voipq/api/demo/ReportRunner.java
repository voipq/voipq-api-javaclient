package nl.voipq.api.demo;

import nl.voipq.api.impl.VoipqApiClient;

public class ReportRunner {
    public static void main(String[] args) {
        new VoipqApiClient()
                .getReportResource()
                .listSalesInvoices(2023, 6)
                .forEach(System.out::println);
    }
}
