package nl.voipq.api.demo;


import nl.voipq.api.domain.*;
import nl.voipq.api.impl.VoipqApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class DidNumberActivateDemo {
    private static final Logger LOG = LoggerFactory.getLogger(DidNumberActivateDemo.class);

    public static void main(String[] args) throws IOException {
        // Lookup first customer
        CustomerInfo customerInfo = new VoipqApiClient()
                .getCustomerResource()
                .listCustomers().get(0);
        CustomerDetail customerDetail =  new VoipqApiClient()
                .getCustomerResource()
                .getCustomer(customerInfo.getUuid());

        // Get offered numbers
        DidNumberOffer offer = new DidNumberOffer();
        offer.setAddress(customerDetail.getAddressList().get(0));
        offer.setBlockSize(10);
        offer.setCountryCode("NL");
        offer.setNetNumber("085");

        List<DidNumber> numbers = new VoipqApiClient()
                .getDidNumberResource()
                .offer(offer);
        if (numbers.isEmpty()) {
            LOG.error("Sorry, sold out for now.");
            return;
        }
        numbers.stream().map(DidNumber::toString).forEach(LOG::info);

        /*
        // Request the first one
        DidNumberRequest request = new DidNumberRequest();
        request.setAddress(offer.getAddress().toReference());
        request.setPhoneNumber(numbers.get(0).getPhoneNumber());
        DidNumber number = new VoipqApiClient()
                .getDidNumberResource()
                .request(request);

        // Get accounts
        AccountDetail account = new VoipqApiClient()
                .getAccountResource()
                .listAccounts().get(0);

        // Activate on first account on first ext
        DidNumberActivate activate = new DidNumberActivate();
        activate.setPhoneNumber(number.getPhoneNumber());
        activate.setAccount(account.toReference());
        activate.setExtension(account.getExtensionList().get(0).getNumber());;

        DidNumber result = new VoipqApiClient()
                .getDidNumberResource()
                .activate(activate);
        LOG.info(result.toString());
        */
    }
}
