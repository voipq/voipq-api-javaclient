package nl.voipq.api.demo;

import nl.voipq.api.impl.VoipqApiClient;

import java.io.IOException;

public class PortingRequestList {
    public static void main(String[] args) {
        new VoipqApiClient()
                .getNumberPortingResource()
                .findAll()
                .forEach(System.out::println);
    }
}
