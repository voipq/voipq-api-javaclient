package nl.voipq.api.demo;


import nl.voipq.api.impl.VoipqApiClient;

import java.io.IOException;

public class AccountList {
    public static void main(String[] args) {
        new VoipqApiClient()
                .getAccountResource()
                .listAccounts()
                .forEach(System.out::println);
    }

}
