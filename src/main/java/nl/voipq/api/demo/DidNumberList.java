package nl.voipq.api.demo;


import nl.voipq.api.impl.VoipqApiClient;

import java.io.IOException;

public class DidNumberList {

    public static void main(String[] args) {
        new VoipqApiClient()
                .getDidNumberResource()
                .findRegionsForCountry("NL")
                .forEach(System.out::println);
    }
}
