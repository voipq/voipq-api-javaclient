package nl.voipq.api.demo;

import nl.voipq.api.impl.VoipqApiClient;

import java.io.IOException;

public class CustomerList {
    public static void main(String[] args) {
        new VoipqApiClient()
                .getCustomerResource()
                .listCustomers()
                .forEach(System.out::println);
    }
}
