package nl.voipq.api.impl;

import nl.voipq.api.domain.AccessToken;
import org.jboss.resteasy.client.jaxrs.internal.BasicAuthentication;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

public class Oauth2Client {
    private static final String OPEN_ID_CONNECT_URL = "https://appauth.nl/realms/voipq/protocol/openid-connect/token";

    /**
     * Read the initial token (does not refresh token) using Client Credentials flow
     */
    public String readTokenClientCredentials(String clientId, String clientSecret) {
        Form form = new Form()
                .param("client_id", clientId)
                .param("client_secret", clientSecret)
                .param("grant_type", "client_credentials");

        AccessToken token = ClientBuilder.newBuilder()
                .build()
    		    .target(OPEN_ID_CONNECT_URL)
                .register(new BasicAuthentication(clientId, clientSecret))
			    .request()
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.form(form), AccessToken.class);
    	return token.getToken();
    }


    /**
     * Read the initial token (does not refresh token) using Direct Access Grants or Resource Owner Password
     * Credentials Grant flow.
     */
    public String readTokenPassword(String username, String password) {
        Form form = new Form()
                .param("grant_type", "password")
                .param("client_id", "voipq-api")
                .param("username", username)
                .param("password", password)
                ;

        AccessToken token = ClientBuilder.newBuilder()
                .build()
    		    .target(OPEN_ID_CONNECT_URL)
			    .request()
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.form(form), AccessToken.class);
    	return token.getToken();
    }

}
