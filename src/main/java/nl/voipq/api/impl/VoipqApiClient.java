package nl.voipq.api.impl;

import nl.voipq.api.demo.CustomerList;
import nl.voipq.api.resource.*;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class VoipqApiClient {

    public CustomerResource getCustomerResource() {
        return buildWebTarget().proxy(CustomerResource.class);
    }

    public AccountResource getAccountResource() {
        return buildWebTarget().proxy(AccountResource.class);
    }

    public NumberPortingResource getNumberPortingResource() {
        return buildWebTarget().proxy(NumberPortingResource.class);
    }

    public ReportResource getReportResource() {
        return buildWebTarget().proxy(ReportResource.class);
    }

    public DidNumberResource getDidNumberResource() {
        return buildWebTarget().proxy(DidNumberResource.class);
    }

    public CallDetailResource getCallDetailResource() {
        return buildWebTarget().proxy(CallDetailResource.class);
    }

    private ResteasyWebTarget buildWebTarget() {
        Properties config = new Properties();
        try (InputStream inputStream = CustomerList.class.getResourceAsStream("/config.properties")) {
            config.load(inputStream);
        } catch (IOException | NullPointerException e) {
            throw new RuntimeException("Error loading config", e);
        }

        boolean directAccess = config.getProperty("api.clientid") == null || config.getProperty("api.clientid").isEmpty();

        Oauth2Client oauth2Client = new Oauth2Client();
        String token;
        if (directAccess) {
            token = oauth2Client.readTokenPassword(
                    config.getProperty("api.username"),
                    config.getProperty("api.password"));
        } else {
            token = oauth2Client.readTokenClientCredentials(
                    config.getProperty("api.clientid"),
                    config.getProperty("api.secret"));
        }

        String url = config.getProperty("api.host") + config.getProperty("api.baseurl");
        return (ResteasyWebTarget) ResteasyClientBuilder
                .newClient()
                .register(new BearerTokenRequestFilter(token))
                .target(url);
    }
}
