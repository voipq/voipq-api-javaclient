package nl.voipq.api.impl;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import java.io.IOException;

public class BearerTokenRequestFilter implements ClientRequestFilter {
    private final String authToken;

    public BearerTokenRequestFilter(String token) {
        this.authToken = token;
    }

    @Override
    public void filter(ClientRequestContext clientRequestContext) {
        clientRequestContext.getHeaders().add("Authorization", "Bearer " + authToken);
    }
}
