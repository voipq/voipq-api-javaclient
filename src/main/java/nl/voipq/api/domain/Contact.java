package nl.voipq.api.domain;

public class Contact {
    private String uuid;
    private String initials;
    private String lastName;
    private String email;
    private String phone;
    private Reference customer;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Reference getCustomer() {
        return customer;
    }

    public void setCustomer(Reference customer) {
        this.customer = customer;
    }
}
