package nl.voipq.api.domain;

public class Reference {
    private String uuid;
    private String name;

    public Reference() {
    }

    public Reference(String uuid) {
        this.uuid = uuid;
        this.name = "";
    }

    public Reference(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
