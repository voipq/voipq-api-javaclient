package nl.voipq.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DidNumber {
    private String phoneNumber;
    private Integer blockSize;
    private String stateDate;
    private String state;
    private CustomerInfo customer;
    private Reference account;

    @Override
    public String toString() {
        return "DidNumber{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", blockSize=" + blockSize +
                ", stateDate='" + stateDate + '\'' +
                ", state='" + state + '\'' +
                ", customer=" + (customer == null ? null : customer.toString()) +
                '}';
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(Integer blockSize) {
        this.blockSize = blockSize;
    }

    public String getStateDate() {
        return stateDate;
    }

    public void setStateDate(String stateDate) {
        this.stateDate = stateDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public CustomerInfo getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerInfo customer) {
        this.customer = customer;
    }

    public Reference getAccount() {
        return account;
    }

    public void setAccount(Reference account) {
        this.account = account;
    }
}
