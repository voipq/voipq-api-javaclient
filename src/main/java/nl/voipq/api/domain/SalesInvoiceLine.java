package nl.voipq.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesInvoiceLine {
    private String description;
    private Double amount;
    private Double setup;

    @Override
    public String toString() {
        return "SalesInvoiceLine{" +
                "description='" + description + '\'' +
                ", amount=" + amount +
                ", setup=" + setup +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getSetup() {
        return setup;
    }

    public void setSetup(Double setup) {
        this.setup = setup;
    }
}
