package nl.voipq.api.domain;

import java.util.Objects;

public class CodedName {
    private String code;
    private String name;

    @Override
    public String toString() {
        return "CodedName{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public CodedName() {
    }

    public CodedName(String code, String name) {
        this.code = code;
        this.name = name;
    }


    public CodedName(String code) {
        this.code = code;
        this.name = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CodedName codedName = (CodedName) o;
        return Objects.equals(code, codedName.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
