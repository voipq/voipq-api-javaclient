package nl.voipq.api.domain;

public class DidNumberRequest {
    private Reference address;
    private String phoneNumber;

    public Reference getAddress() {
        return address;
    }

    public void setAddress(Reference address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
