package nl.voipq.api.domain;

public class CallDetailResponse {
    private String id;
    private String clientName;
    private String relationNumber;
    private String service;
    private String flow;
    private String extensionNumber;
    private String calledNumber;
    private String callingNumber;
    private String callerIdDigits;
    private String callerIdRaw;
    private Long callStart;
    private String destPrefix;
    private String destGroupName;
    private String destCountry;
    private String disposition;
    private Integer chargedUnitCount;
    private String chargedUnitType;
    private Double chargedTotal;
    private Double chargedTotalSale;

    @Override
    public String toString() {
        return "CallDetailResponse{" +
                "id='" + id + '\'' +
                ", service='" + service + '\'' +
                ", callingNumber='" + callingNumber + '\'' +
                ", calledNumber='" + calledNumber + '\'' +
                ", chargedUnitCount='" + chargedUnitCount + '\'' +
                ", chargedUnitType='" + chargedUnitType + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getRelationNumber() {
        return relationNumber;
    }

    public void setRelationNumber(String relationNumber) {
        this.relationNumber = relationNumber;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getExtensionNumber() {
        return extensionNumber;
    }

    public void setExtensionNumber(String extensionNumber) {
        this.extensionNumber = extensionNumber;
    }

    public String getCalledNumber() {
        return calledNumber;
    }

    public void setCalledNumber(String calledNumber) {
        this.calledNumber = calledNumber;
    }

    public String getCallingNumber() {
        return callingNumber;
    }

    public void setCallingNumber(String callingNumber) {
        this.callingNumber = callingNumber;
    }

    public String getCallerIdDigits() {
        return callerIdDigits;
    }

    public void setCallerIdDigits(String callerIdDigits) {
        this.callerIdDigits = callerIdDigits;
    }

    public String getCallerIdRaw() {
        return callerIdRaw;
    }

    public void setCallerIdRaw(String callerIdRaw) {
        this.callerIdRaw = callerIdRaw;
    }

    public Long getCallStart() {
        return callStart;
    }

    public void setCallStart(Long callStart) {
        this.callStart = callStart;
    }

    public String getDestPrefix() {
        return destPrefix;
    }

    public void setDestPrefix(String destPrefix) {
        this.destPrefix = destPrefix;
    }

    public String getDestGroupName() {
        return destGroupName;
    }

    public void setDestGroupName(String destGroupName) {
        this.destGroupName = destGroupName;
    }

    public String getDestCountry() {
        return destCountry;
    }

    public void setDestCountry(String destCountry) {
        this.destCountry = destCountry;
    }

    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    public Integer getChargedUnitCount() {
        return chargedUnitCount;
    }

    public void setChargedUnitCount(Integer chargedUnitCount) {
        this.chargedUnitCount = chargedUnitCount;
    }

    public String getChargedUnitType() {
        return chargedUnitType;
    }

    public void setChargedUnitType(String chargedUnitType) {
        this.chargedUnitType = chargedUnitType;
    }

    public Double getChargedTotal() {
        return chargedTotal;
    }

    public void setChargedTotal(Double chargedTotal) {
        this.chargedTotal = chargedTotal;
    }

    public Double getChargedTotalSale() {
        return chargedTotalSale;
    }

    public void setChargedTotalSale(Double chargedTotalSale) {
        this.chargedTotalSale = chargedTotalSale;
    }
}
