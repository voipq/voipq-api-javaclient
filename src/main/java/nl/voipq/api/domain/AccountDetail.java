package nl.voipq.api.domain;

import java.util.List;
import java.util.stream.Collectors;

public class AccountDetail {
    private String uuid;
    private CustomerInfo customer;
    private String identifier;
    private String type;
    private String extensionNo;
    private String name;
    private List<AccountExtension> extensionList;

    public Reference toReference() {
        return new Reference(uuid, identifier);
    }

    @Override
    public String toString() {
        return "AccountDetail{" +
                "uuid='" + uuid + '\'' +
                ", customer=" + customer +
                ", identifier='" + identifier + '\'' +
                ", type='" + type + '\'' +
                ", extensionList=" + (extensionList == null ? "null"
                    : extensionList.stream().map(AccountExtension::getNumber).collect(Collectors.joining(","))) +
                '}';
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public CustomerInfo getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerInfo customer) {
        this.customer = customer;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExtensionNo() {
        return extensionNo;
    }

    public void setExtensionNo(String extensionNo) {
        this.extensionNo = extensionNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AccountExtension> getExtensionList() {
        return extensionList;
    }

    public void setExtensionList(List<AccountExtension> extensionList) {
        this.extensionList = extensionList;
    }
}
