package nl.voipq.api.domain;

public class AccountExtension {
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
