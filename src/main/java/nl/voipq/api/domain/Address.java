package nl.voipq.api.domain;

public class Address {
    private String uuid;
    private String street;
   	private String houseNumber;
   	private String numberPostfix;
    private String city;
   	private String postcode;
   	private String countryCode;
    private Reference customer;

    public Reference toReference() {
        return new Reference(uuid, postcode + " " + houseNumber + numberPostfix + " " + countryCode);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getNumberPostfix() {
        return numberPostfix;
    }

    public void setNumberPostfix(String numberPostfix) {
        this.numberPostfix = numberPostfix;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Reference getCustomer() {
        return customer;
    }

    public void setCustomer(Reference customer) {
        this.customer = customer;
    }
}
