package nl.voipq.api.domain;

public class PortingRequestNumber {
    private Integer item;
    private String numberStart;
    private String numberEnd;
    private Integer blockingCode;
    private String blockingNote;

    public Integer getItem() {
        return item;
    }

    public void setItem(Integer item) {
        this.item = item;
    }

    public String getNumberStart() {
        return numberStart;
    }

    public void setNumberStart(String numberStart) {
        this.numberStart = numberStart;
    }

    public String getNumberEnd() {
        return numberEnd;
    }

    public void setNumberEnd(String numberEnd) {
        this.numberEnd = numberEnd;
    }

    public Integer getBlockingCode() {
        return blockingCode;
    }

    public void setBlockingCode(Integer blockingCode) {
        this.blockingCode = blockingCode;
    }

    public String getBlockingNote() {
        return blockingNote;
    }

    public void setBlockingNote(String blockingNote) {
        this.blockingNote = blockingNote;
    }
}
