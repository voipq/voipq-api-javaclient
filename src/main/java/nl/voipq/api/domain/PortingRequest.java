package nl.voipq.api.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PortingRequest {
    private Date requestDate;
    private CustomerInfo customer;
    private String portingID;

    private CodedName donorServiceProvider;
    private CodedName recipientServiceProvider;
    private CodedName donorNetworkOperator;
    private CodedName recipientNetworkOperator;

    private List<PortingRequestNumber> numbers;
    private Address address;
    private Contact contact;
    private PortingProcess process;
    private String numberType;
    private String requestNote;
    private String internalNote;

    private CodedName state;
    private Date stateDate;

    private String changeNote;
    private Date changeDate;

    private String changeAnswerNote;
    private Date changeAnswerDate;

    @Override
    public String toString() {
        return "PortingRequest{" +
                "requestDate=" + requestDate +
                ", customer=" + customer +
                ", portingID='" + portingID + '\'' +
                ", donorServiceProvider='" + donorServiceProvider + '\'' +
                ", recipientServiceProvider='" + recipientServiceProvider + '\'' +
                ", donorNetworkOperator='" + donorNetworkOperator + '\'' +
                ", recipientNetworkOperator='" + recipientNetworkOperator + '\'' +
                ", numbers=" + (numbers == null ? "null"
                    : numbers.stream().map(PortingRequestNumber::toString).collect(Collectors.joining(","))) +
                ", address=" + (address == null ? "null" : address.toString()) +
                ", contact=" + (contact == null ? "null" : contact.toString()) +
                '}';
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public CustomerInfo getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerInfo customer) {
        this.customer = customer;
    }

    public String getPortingID() {
        return portingID;
    }

    public void setPortingID(String portingID) {
        this.portingID = portingID;
    }

    public CodedName getDonorServiceProvider() {
        return donorServiceProvider;
    }

    public void setDonorServiceProvider(CodedName donorServiceProvider) {
        this.donorServiceProvider = donorServiceProvider;
    }

    public CodedName getRecipientServiceProvider() {
        return recipientServiceProvider;
    }

    public void setRecipientServiceProvider(CodedName recipientServiceProvider) {
        this.recipientServiceProvider = recipientServiceProvider;
    }

    public CodedName getDonorNetworkOperator() {
        return donorNetworkOperator;
    }

    public void setDonorNetworkOperator(CodedName donorNetworkOperator) {
        this.donorNetworkOperator = donorNetworkOperator;
    }

    public CodedName getRecipientNetworkOperator() {
        return recipientNetworkOperator;
    }

    public void setRecipientNetworkOperator(CodedName recipientNetworkOperator) {
        this.recipientNetworkOperator = recipientNetworkOperator;
    }

    public List<PortingRequestNumber> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<PortingRequestNumber> numbers) {
        this.numbers = numbers;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public PortingProcess getProcess() {
        return process;
    }

    public void setProcess(PortingProcess process) {
        this.process = process;
    }

    public String getNumberType() {
        return numberType;
    }

    public void setNumberType(String numberType) {
        this.numberType = numberType;
    }

    public String getRequestNote() {
        return requestNote;
    }

    public void setRequestNote(String requestNote) {
        this.requestNote = requestNote;
    }

    public String getInternalNote() {
        return internalNote;
    }

    public void setInternalNote(String internalNote) {
        this.internalNote = internalNote;
    }

    public CodedName getState() {
        return state;
    }

    public void setState(CodedName state) {
        this.state = state;
    }

    public Date getStateDate() {
        return stateDate;
    }

    public void setStateDate(Date stateDate) {
        this.stateDate = stateDate;
    }

    public String getChangeNote() {
        return changeNote;
    }

    public void setChangeNote(String changeNote) {
        this.changeNote = changeNote;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public String getChangeAnswerNote() {
        return changeAnswerNote;
    }

    public void setChangeAnswerNote(String changeAnswerNote) {
        this.changeAnswerNote = changeAnswerNote;
    }

    public Date getChangeAnswerDate() {
        return changeAnswerDate;
    }

    public void setChangeAnswerDate(Date changeAnswerDate) {
        this.changeAnswerDate = changeAnswerDate;
    }
}
