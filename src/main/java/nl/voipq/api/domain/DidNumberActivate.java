package nl.voipq.api.domain;

public class DidNumberActivate {
    private String phoneNumber;
    private Reference account;
    private String extension;

    @Override
    public String toString() {
        return "DidNumberActivate{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", account=" + account +
                ", extension='" + extension + '\'' +
                '}';
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Reference getAccount() {
        return account;
    }

    public void setAccount(Reference account) {
        this.account = account;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}