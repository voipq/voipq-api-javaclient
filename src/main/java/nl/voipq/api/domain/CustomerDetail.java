package nl.voipq.api.domain;


import java.util.List;

public class CustomerDetail {
    private String uuid;
    private String name;
    private boolean active;
    private Integer relationNumber;
    private String created;
    private List<Address> addressList;
    private List<Contact> contactList;
    private List<DidNumber> numberList;

    @Override
    public String toString() {
        return "CustomerDetail{" +
                "uuid='" + uuid + '\'' +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", relationNumber=" + relationNumber +
                ", created='" + created + '\'' +
                ", addressList=" + addressList +
                ", contactList=" + contactList +
                '}';
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getRelationNumber() {
        return relationNumber;
    }

    public void setRelationNumber(Integer relationNumber) {
        this.relationNumber = relationNumber;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }

    public List<DidNumber> getNumberList() {
        return numberList;
    }

    public void setNumberList(List<DidNumber> numberList) {
        this.numberList = numberList;
    }
}
