package nl.voipq.api.domain;

/**
 * Created by thiadmer on 05/02/2017.
 */
public enum PortingProcess {
    I("In port"),
    O("Out port"),
    A("Activate");

    private String name;

    PortingProcess(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }
}

