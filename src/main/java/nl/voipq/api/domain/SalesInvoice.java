package nl.voipq.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesInvoice {
    private String debtorNumber;
    private String debtorName;
    private List<SalesInvoiceLine> lines;

    @Override
    public String toString() {
        return "SalesInvoice{" +
                "debtorNumber='" + debtorNumber + '\'' +
                ", debtorName='" + debtorName + '\'' +
                ", lines=" + lines.stream().map(Objects::toString).collect(Collectors.joining(",")) +
                '}';
    }

    public String getDebtorNumber() {
        return debtorNumber;
    }

    public void setDebtorNumber(String debtorNumber) {
        this.debtorNumber = debtorNumber;
    }

    public String getDebtorName() {
        return debtorName;
    }

    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    public List<SalesInvoiceLine> getLines() {
        return lines;
    }

    public void setLines(List<SalesInvoiceLine> lines) {
        this.lines = lines;
    }
}
